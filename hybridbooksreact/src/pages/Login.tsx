import React from 'react'
import FormLogin from '../component/forms/login/FormLogin'
const Login = () => {
  return (
    <div>
        <h1 className="heading">Hybrid Books</h1>
        <FormLogin/>
    </div>
  )
}

export default Login
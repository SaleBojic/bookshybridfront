import React from 'react'
import { useParams } from 'react-router'
import CreateUpdateBookForm from '../component/books/createBook/CreateUpdateBookForm';

const UpdateBook = () => {
    const {id} = useParams();
    console.log(Number(id));
  return (
    <div>
        <h2>Edit Book</h2>
        <CreateUpdateBookForm bookId={Number(id)}/>
    </div>
    
  )
}

export default UpdateBook
import React, { useEffect } from 'react'
import BookCard from '../component/books/BookCard';
import {Book} from '../model/Book'
import './styles.css'
import { useAppDispatch, useAppSelector } from '../redux/hooks';
import { BookState } from '../redux/store';
import { getAllBooks } from '../services/BookAPI';
import { setBooks } from '../redux/store';


const Books = () => {
  
  const dispatch = useAppDispatch();
  const {books} = useAppSelector<BookState>(state => state.bookReducer);
  useEffect(() => {
    getAllBooks().then((res) => {
    dispatch(setBooks(res.data));
  }) 
  },[]);


  return (
    <div className="books-container">
        {books.map((book:Book) => <BookCard key={book.id} book={book} />)}
    </div>
  )
}

export default Books
import React, { useEffect, useState } from 'react'

import { useParams } from 'react-router'
import { getBookById } from '../services/BookAPI';
import { Book } from '../model/Book';
import BookCard from '../component/books/BookCard';


const SelectedBook = () => {
    const [selectedBook,setSelectedBook] = useState<Book>();
    const {id} = useParams();
    
    useEffect(() => {
        getBookById(Number(id)).then(response => {
            setSelectedBook(response.data);
        });
    }, []);

  return (
    <div>
        {selectedBook && <BookCard book={selectedBook} />}
    </div>
  )
}

export default SelectedBook
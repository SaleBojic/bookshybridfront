import React from 'react'

import CreateUpdateBookForm from '../component/books/createBook/CreateUpdateBookForm'

const CreateUpdateBooks = () => {
  return (
    <div>
        <h1 className="heading">Hybrid Books</h1>
        <CreateUpdateBookForm/>
    </div>
  )
}

export default CreateUpdateBooks
import axios from 'axios'
import { Book } from '../model/Book';
import { getAuthToken } from './Auth';

export const getAllBooks = () => {
    return axios.get('http://54.93.246.187:8080/api/v3/books');
}

export const getBookById = (id:number) =>{ 
    return axios.get(`http://54.93.246.187:8080/api/v3/books/${id}`, {headers: { Authorization: 'Bearer ' + getAuthToken() }});
}

export const createBook = (book:Book) => {
   return axios.post('http://54.93.246.187:8080/api/v3/books', book, {headers: { Authorization: 'Bearer ' + getAuthToken() }});
}

export const updateBook = (id: number, book: Book) => {
    return axios.put(`http://54.93.246.187:8080/api/v3/books/${id}`, book, {headers: { Authorization: 'Bearer ' + getAuthToken() }});
}

export const deleteBook = (id:number) => {
    return axios.delete(`http://54.93.246.187:8080/api/v3/books/${id}`, {headers: { Authorization: 'Bearer ' + getAuthToken() }});
}
import axios from 'axios'

export const login = (username: string, password: string ) => {
    return axios.post('http://54.93.246.187:8080/api/v3/auth/login', {
        username,
        password
    });
}


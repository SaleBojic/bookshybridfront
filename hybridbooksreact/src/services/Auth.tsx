export const logout = () => {
    localStorage.removeItem('user');
}

export const getAuthToken = () => {
    if(localStorage.getItem('user')){
        const user = JSON.parse(localStorage.getItem('user')!);
        return user.token;
    }else{
        return '';
    }
}
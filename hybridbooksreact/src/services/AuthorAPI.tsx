import axios from 'axios'
import { Author } from '../model/Author';
import { getAuthToken } from './Auth';


const URL = 'http://54.93.246.187:8080/api/v3/authors';

export const getAllAuthors = () =>{
    return axios.get(`${URL}`, {headers: { Authorization: 'Bearer ' + getAuthToken() }});
}

export const getAuthorById = (authorId: number) =>{
    return axios.get(`http://54.93.246.187:8080/api/v3/authors/${authorId}`, {headers: { Authorization: 'Bearer ' + getAuthToken() }});
}

export const createAuthor = (author: Author) =>{
   return axios.post(`http://54.93.246.187:8080/api/v3/authors` , author , {headers: { Authorization: 'Bearer ' + getAuthToken() }});
}

export const updateAuthor = (author: Author) =>{
    return axios.put(`${URL}`, author, {headers: { Authorization: 'Bearer ' + getAuthToken() }});
}
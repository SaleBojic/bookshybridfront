import './App.css';
import React from 'react';
import {BrowserRouter, Route, Routes} from "react-router-dom"
import { Navbar } from './component/navbar/Navbar';
import Books from './pages/Books';
import Login from './pages/Login';
import CreateBooks from './pages/CreateBooks';
import { store } from './redux/store';
import { Provider } from 'react-redux';
import UpdateBook from './pages/UpdateBook';
import SelectedBook from './pages/SelectedBook';

const App = () => {  
  return (
    <div className="App">
      <Provider store={store}>
      <BrowserRouter>
      <Navbar/>
      <Routes>
        <Route path='/' element={<Books/>}/>
        <Route path='/login' element={<Login/>}/>
        <Route path='/create' element={<CreateBooks/>}/>
        <Route path='/update-book/:id' element={<UpdateBook/>}/>
        <Route path='/selected-book/:id' element={<SelectedBook/>}/>
      </Routes>
      </BrowserRouter>
      </Provider> 
    </div>
  );
}

export default App;

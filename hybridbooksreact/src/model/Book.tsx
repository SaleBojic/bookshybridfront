import moment, { Moment } from "moment"
import { Author } from "./Author"

export interface Book {
    id?: number
    title: string
    description: string
    isbn: string
    quantity: number
    creationDate: string
    imageUrl?: string
    authors: Author[]
  }
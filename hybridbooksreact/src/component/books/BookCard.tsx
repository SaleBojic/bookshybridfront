import React from 'react'
import { Link } from 'react-router-dom';
import {Book} from '../../model/Book'
import { useAppDispatch } from '../../redux/hooks';
import { deleteBooks } from '../../redux/store';
import { deleteBook } from '../../services/BookAPI';
import './style.scss'

const BookCard = (props: {book: Book}) => {
    const book = props.book;

    const dispatch = useAppDispatch();
    const handleDelete = () => {
      console.log('triggered hanlde delete');
      if(book.id){
        deleteBook(book.id).then(() => {
          const id = Number(book.id)
          dispatch(deleteBooks({n: id}));
          alert("deleted succesfully");
        }).catch( error => {
          alert(`book was not deleted ${error.response.data}`);
        })
      }
    }
    let replacementImgUrl = book.imageUrl ? book.imageUrl : 'https://anylang.net/sites/default/files/covers/harry-potter-and-order-phoenix.jpg';
    if(book.imageUrl?.includes('localhost')){
      replacementImgUrl = 'https://anylang.net/sites/default/files/covers/harry-potter-and-order-phoenix.jpg';
    }
    return  (
    <div className="book-card" id="book-list" key={book.id} >
      <button className='book-card__delete' onClick={handleDelete}>X</button>
      <h2 className='book-card__title'>{book.title}</h2> 
      <img className='book-card__img' src={replacementImgUrl}/>
      <p className='book-card__description'>{book.description}</p>
      <button className='book-card__button'>Rent</button>
      <button className='book-card__button book-card__button__update'><Link to={`/update-book/${book.id}`}>Update</Link></button>
      <Link to={`/selected-book/${book.id}`}><p>details</p></Link>
    </div>
    )
}

export default BookCard
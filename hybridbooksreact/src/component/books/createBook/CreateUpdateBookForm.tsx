import React, { useEffect } from 'react'
import './styles.css'
import {useState} from 'react';
import moment from 'moment'
import { createBook, getBookById, updateBook } from '../../../services/BookAPI';
import { Author } from '../../../model/Author';
import { getAllAuthors } from '../../../services/AuthorAPI';
import { Book } from '../../../model/Book';
import CreateAuthor from '../../author/CreateAuthor';

const CreateUpdateBookForm = (props : {bookId?: number }) => {

    const[title, setTitle] = useState<string>('Lord of the rings');
    const[description, setDesc] = useState<string>('Lord of the rings is great book with greate charracters');
    const[isbn,setIsbn] = useState<string>('123-123-123-123-1');
    const[quantity,setQty] = useState<number>(2);
    const[imgUrl,setImgUrl] = useState<string>('https://tralala.com?img=1');
    const[authors,setAuthors] = useState<Author[]>([]);
    const[selectedAuthors,setSelectedAuthors] = useState<Author[]>([]);
    const[buttonText,setButtonText] = useState<string>('Create');
    const bookId = props.bookId;

    useEffect(() => {
      if (bookId) {
          getBookById(bookId).then(response => {
              const data = response.data;
              setTitle(data.title);
              setDesc(data.description);
              setIsbn(data.isbn);
              setQty(data.quantity);
              setImgUrl(data.imageUrl);
              setAuthors(data.authors);
              setButtonText('Update');
          });
      }
      getAllAuthors().then(response => {
        setAuthors(response.data);
      });
    }, []);
    

    const handleOnChangeInput = (event: any) => {
        let eventName = event.target.name;
        let eventValue = event.target.value;
        if(eventName == 'title') setTitle(eventValue);
        if(eventName == 'desc') setDesc(eventValue);
        if(eventName == 'isbn') setIsbn(eventValue);
        if(eventName == 'qty') setQty(eventValue);
        if(eventName == 'imgUrl') setImgUrl(eventValue);
    }

    const handleSubmit = (event:any) => {
        event.preventDefault();
        if (props?.bookId) {
            const book: Book = {
              id: Number(props?.bookId),
              title,
              description,
              isbn,
              quantity,
              creationDate: moment(new Date()).format('YYYY-MM-DD'),
              imageUrl: imgUrl,
              authors: authors
            }
            book.authors = selectedAuthors
            updateBook(props?.bookId, book).then(() => {
              alert('Book updated!');
            }).catch(error => {
              alert('Updated failed Error: ' + error.response.data);
            })
          } else {
            const book: Book = {
              title,
              description,
              isbn,
              quantity,
              creationDate: moment(new Date()).format('YYYY-MM-DD'),
              imageUrl: imgUrl,
              authors: authors
            }
            book.authors = selectedAuthors;
            createBook(book).then(() => {
              alert('Book created successfully')
            }).catch(error => {
              alert('Failed to create book. Error: ' + error.response.data)
            })
          }
        }
      
    const handleChangeSelect = (event:any, author: Author) => {
        const exists = selectedAuthors.some(a => a.id == author.id)
        if(!exists){
            setSelectedAuthors([...selectedAuthors, author])
        }
        else {
        const index = selectedAuthors.indexOf(author)
        selectedAuthors.splice(index, 1)
        setSelectedAuthors([...selectedAuthors])
        }
    }

  return (
    <div className='create-book-conteiner'>
        <h2>Create New Book</h2>
        <form className='create-book-form' onSubmit={handleSubmit}>
            <label className='create-book-form__label'>Title</label>
            <input  type='text' placeholder={title} value={title} onChange={handleOnChangeInput} name='title'/>
            <label className='create-book-form__label'>Description</label>
            <input type='text' placeholder={description} value={description} onChange={handleOnChangeInput} name='desc'/>
            <label className='create-book-form__label'>ISBN</label>
            <input type='text' placeholder={isbn} value={isbn} onChange={handleOnChangeInput} name='isbn'/>
            <label className='create-book-form__label'>Quantity</label>
            <input type='number' value={quantity} onChange={handleOnChangeInput} name='qty'/>
            <label className='create-book-form__label'>Image URL</label>
            <input type='text' value={imgUrl} onChange={handleOnChangeInput} name='imgUrl'/>
        <div className='select-authors'>
            <ul onClick={(event) => {event.stopPropagation()}}>
                {authors.map((author) => (<li key={author.id}>
                    <div>
                        <input type='checkbox' checked={selectedAuthors.some(a => a.id === author.id)} onChange={(e) => handleChangeSelect(e, author)} />
                        {author.firstName} {author.middleName} {author.lastName}
                    </div>
                </li>))}
            </ul> 
        </div>
            <button type='submit' >{buttonText}</button>
        </form>
        <CreateAuthor/>
    </div>
  )
}


export default CreateUpdateBookForm
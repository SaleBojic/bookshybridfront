import React from 'react'
import "./styles.css";

const SearchFilter = () => {
  return (
    <form className='input'>
        <input type='input' placeholder='Search here' className='input__box' />
        <button type='submit' className='input__submit fa fa-search'></button>   
    </form>
  )
}

export default SearchFilter
import React from 'react'
import './styles.css'
import {useState} from 'react';
import SearchFilter from './searchFilter/SearchFilter';
import { Link } from 'react-router-dom';
import { useAppDispatch} from '../../redux/hooks';
import { logout } from '../../services/Auth';
import { removeRoles } from '../../redux/store';
export const Navbar = () => {

  const[isClicked, setClicked] = useState(true);
  const dispatch = useAppDispatch();
  const changeClassOnClick = () => {
    setClicked(!isClicked);
  }
  const logOut = () => {
    logout();
    dispatch(removeRoles());
  }
  
  const showMenu: string = isClicked ? "navbar-conteiner__hide-menu" : "navbar-conteiner__show-menu";
  return (
    <div className='navbar-conteiner'>
    <div className='navbar-conteiner__navbar'>
      <SearchFilter/>
      <button className='navbar-conteiner__hamburger' onClick={changeClassOnClick}>
        <i className='fa fa-bars'></i>
      </button>
    </div>
    <div className='navbar-conteriner__list-conteiner'>
      <ul className={`navbar-conteiner__list ${showMenu}`}>
          <li><Link to="/">Books</Link></li>
          <li><Link to="/create">New Book</Link></li>
          <li><Link to="/login">Login</Link></li>
          <li onClick={logOut}><Link to="/login">Log Out</Link></li>
      </ul>
      </div>
    </div>
  )
}

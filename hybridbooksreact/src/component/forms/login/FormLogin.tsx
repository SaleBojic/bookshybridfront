import React from 'react'
import './styles.css'
import { useAppDispatch} from '../../../redux/hooks';
import { useState } from 'react'
import jwt from 'jwt-decode'
import { login } from '../../../services/UserApi'
import { IJWTToken } from '../../../model/JWTToken'
import { setRoles } from '../../../redux/store'


const FormLogin = () => {
  const[username,setUsername] = useState<string>('');
  const[password,setPassword] = useState<string>('');

  const handleChangeInput = (event: any) =>{
    const name = event.target.name;
    const value = event.target.value;
    if(name === 'username') setUsername(value);
    if(name === 'password') setPassword(value);
  }

  const dispatch = useAppDispatch();
  const handleSubmit = (event: any) => {
    event.preventDefault();

    login(username,password)
    .then(response => {
      if(response?.data){
        const decodedToken : IJWTToken = jwt<IJWTToken>(response.data.token)
        dispatch(setRoles(decodedToken.role));
        localStorage.setItem('user', JSON.stringify(response.data));
      } 
    });
  }
  
  return (
    <div className='login-conteiner'>
      <form className='form-login' onSubmit={handleSubmit}>
        <label className='form-login__label'>Username: </label>
        <input type="text" className='form-login__input' name='username' value={username} onChange={handleChangeInput}/>
        <label className='form-login__label'>Password: </label>
        <input type="password" className='form-login__input' name='password' value={password} onChange={handleChangeInput} />
        <button className='form-login__submit' type="submit">Log in</button>
        <a href="#">Forgot Password?</a>
      </form>
    </div>
  )
}

export default FormLogin;
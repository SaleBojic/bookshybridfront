import React from 'react'
import { useState, useEffect } from 'react'
import { Author } from '../../model/Author';
import { createAuthor, getAuthorById, updateAuthor } from '../../services/AuthorAPI';
import './styles.css'

const CreateAuthor = (props: {authorId? : number}) => {

  const[formOpen,setFormOpen] = useState(false);
  const[firstName,setFirstName] = useState<string>('Kia');
  const[middleName,setMiddleName] = useState<string>('Brate');
  const[lastName,setLastName] = useState<string>('Kockar');

  const openFormOnClick = () => {
    setFormOpen(!formOpen);
  }
  
  const handleInputChange = (event: any) => {
    const eventName = event.target.name;
    const eventValue = event.target.value;
    if(eventName == 'firstName') setFirstName(eventValue);
    if(eventName == 'middleName') setMiddleName(eventValue);
    if(eventName == 'lastName') setLastName(eventValue);
  }

  useEffect(() => {
    if(props.authorId){
      getAuthorById(props.authorId).then(response => {
        const data = response.data;
        setFirstName(data.firstName);
        setMiddleName(data.middleName);
        setLastName(data.lastName);
      });
    }
  },[]);

  const handleSubmit = (event: any) => {
    event.preventDefault();
    if(props.authorId){
      const author: Author = {
        id: Number(props.authorId),
        firstName: firstName,
        middleName: middleName,
        lastName: lastName
      }
      updateAuthor(author).then( () =>
        alert("author updated")
      ).catch( error => 
        alert(`author update failed ${error.response.data}`)
      )
    }else{
      const author: Author = {
        firstName: firstName,
        middleName: middleName,
        lastName: lastName
      }
      createAuthor(author).then(() => {
        alert('Author created!');
      }).catch(error => {
        alert(`Failed to create author Error: ${error.response.data}`);
      })
    }
  }
  const showAuthorForm: string = formOpen ? 'author-form__show' : 'author-form__hide';
  return (
    <div className='author-form-conteiner'>
        <button className='author-form__new-author-button' onClick={openFormOnClick}>Add New Author</button>
        <form className={`author-form ${showAuthorForm}`} onSubmit={handleSubmit} >
            <label>First Name:</label>
            <input type='text'  value={firstName} onChange={handleInputChange} name='firstName'/>
            <label>Middle Name:</label>
            <input type='text'  value={middleName} onChange={handleInputChange} name='middleName'/>
            <label>Last Name:</label>
            <input type='text'  value={lastName} onChange={handleInputChange} name='lastName'/>
            <button type='submit'>Create Author</button>
        </form>
    </div>
  )
}

export default CreateAuthor
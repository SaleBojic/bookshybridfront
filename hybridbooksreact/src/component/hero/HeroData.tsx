export interface HeroData{
    title: string,
    description: string,
    subtitle?: string,
    buttonText: string
}

import React from 'react'
import './styles.css'
import {HeroData} from './HeroData'

const HeroImageCover = (props: {data: HeroData}) => {
  const {title,description,subtitle,buttonText}:HeroData = props.data;
  return (
    <div className="hero-image">
    <div className="hero-image__text">
        <h1>{title}</h1>
        <hr/>
        <p>{description}</p>
        <h2>{subtitle}</h2>
        <button className="hero-image__button"><a href="#book-list">{buttonText}</a></button>
    </div>
    </div>
  )
}

export default HeroImageCover
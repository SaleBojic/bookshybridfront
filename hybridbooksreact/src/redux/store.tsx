import { configureStore, createSlice, PayloadAction } from '@reduxjs/toolkit'
import { Book } from '../model/Book'

export interface BookState{
  books: Book[]
}

const initialStateBook: BookState = {
  books: []
}

export interface AuthState{
  roles: string[]
}

const initialStateAuth: AuthState = {
  roles: []
}

const booksSlice = createSlice({
  name:'books',
  initialState: initialStateBook,
  reducers: {
    setBooks(state, { payload }: PayloadAction<Book[]>) {
      state.books = [ ...payload ]
    },
    deleteBooks(state, {payload}: PayloadAction<{n: number}>){
      state.books = state.books.filter(({id}) => id !== payload.n);
    }
  }
})

const authSlice = createSlice({
  name:'auth',
  initialState: initialStateAuth,
  reducers: {
    setRoles(state, { payload}: PayloadAction<string[]>){
      state.roles = payload;
    },
    removeRoles(state){
      state.roles = [];
    }
  }
})

export const store = configureStore({
  reducer: {
    bookReducer: booksSlice.reducer,
    authReducer: authSlice.reducer,
  }
})

export const { setRoles , removeRoles} = authSlice.actions;
export const { setBooks , deleteBooks } = booksSlice.actions;
export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch